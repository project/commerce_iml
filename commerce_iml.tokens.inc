<?php

/**
 * @file
 * Builds placeholder replacement tokens for order-related data.
 */


/**
 * Implements hook_token_info_alter().
 */
function commerce_iml_token_info_alter(&$data) {
  $data['tokens']['commerce-order']['order-payment-balance-amount-raw'] = array(
    'name' => t('Order payment balance Raw amount'),
    'description' => t('The raw amount of the balance of payment transactions of this order.'),
  );
  $data['tokens']['commerce-order']['order-payment-balance-amount-decimal'] = array(
    'name' => t('Order payment balance Raw amount'),
    'description' => t('The raw amount of the balance of payment transactions of this order.'),
  );
  $data['tokens']['commerce-order']['order-payment-balance-amount-formatted'] = array(
    'name' => t('Order payment balance Formatted amount'),
    'description' => t('The formatted amount of the balance of payment transactions of this order.'),
  );
}

/**
 * Implements hook_tokens().
 */
function commerce_iml_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'order-payment-balance-amount-raw':
          $order_balance = _commerce_iml_tokens_get_order_balance($order);
          $replacements[$original] = $sanitize ? check_plain($order_balance['amount']) : $order_balance['amount'];
          break;
        case 'order-payment-balance-amount-decimal':
          $order_balance = _commerce_iml_tokens_get_order_balance($order);
          $amount_decimal = commerce_currency_amount_to_decimal($order_balance['amount'], $order_balance['currency_code']);
          $replacements[$original] = $sanitize ? check_plain($amount_decimal) : $amount_decimal;
          break;
        case 'order-payment-balance-amount-formatted':
          $order_balance = _commerce_iml_tokens_get_order_balance($order);
          $balance_formatted = commerce_currency_format($order_balance['amount'], $order_balance['currency_code']);
          $replacements[$original] = $sanitize ? check_plain($balance_formatted) : $balance_formatted;
          break;
      }
    }
  }

  return $replacements;
}

function _commerce_iml_tokens_get_order_balance($order) {
  if (module_exists('commerce_payment')) {
    $order_balance = commerce_payment_order_balance($order);
  }
  else {
    commerce_order_calculate_total($order);
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_balance = $order_wrapper->commerce_order_total->value();
  }

  return $order_balance;
}