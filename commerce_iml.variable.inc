<?php

function commerce_iml_variable_group_info() {
  $groups = array();

  $groups['commerce_iml_connection_settings'] = array(
    'title' => t('Connection settings'),
    'access' => 'imlogistic connection settings',
  );
  $groups['commerce_iml_order_tokens'] = array(
    'title' => t('Order info tokens settings'),
    'access' => 'imlogistic order tokens settings',
  );
  $groups['commerce_iml_pod_settings'] = array(
    'title' => t('Points of Delivery shipping settings'),
    'access' => 'imlogistic pod shipping settings',
  );
  $groups['commerce_iml_post_settings'] = array(
    'title' => t('Post shipping settings'),
    'access' => 'imlogistic post shipping settings',
  );
  $groups['commerce_iml_order_line_items'] = array(
    'title' => t('Order info line items settings'),
    'access' => 'imlogistic order line items settings',
  );

  return $groups;
}

function commerce_iml_variable_info() {
  $variables = array();

  $variables['commerce_iml_server_api_url'] = array(
    'title' => t('Server Json API url'),
    'group' => 'commerce_iml_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 20,
    ),
    'default' => 'https://api.iml.ru/Json/',
    'type' => 'url',
    'token' => TRUE,
    'access' => 'imlogistic connection settings',
  );
  $variables['commerce_iml_server_lists_url'] = array(
    'title' => t('Server API Dictionaries Lists Url'),
    'group' => 'commerce_iml_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 20,
    ),
    'default' => 'https://list.iml.ru/',
    'type' => 'url',
    'token' => TRUE,
    'access' => 'imlogistic connection settings',
  );
  $variables['commerce_iml_server_timeout'] = array(
    'title' => t('Connection timeout'),
    'group' => 'commerce_iml_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#field_suffix' => t('sec'),
    ),
    'type' => 'number',
    'default' => 60,
    'token' => TRUE,
    'access' => 'imlogistic connection settings',
  );
  $variables['commerce_iml_server_login'] = array(
    'title' => t('Login'),
    'group' => 'commerce_iml_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 8,
    ),
    'type' => 'string',
    'token' => TRUE,
    'access' => 'imlogistic connection settings',
  );
  $variables['commerce_iml_server_password'] = array(
    'title' => t('Password'),
    'group' => 'commerce_iml_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 8,
    ),
    'type' => 'string',
    'token' => TRUE,
    'access' => 'imlogistic connection settings',
  );
  $variables['commerce_iml_server_is_test_mode'] = array(
    'title' => t('Test Mode'),
    'group' => 'commerce_iml_connection_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'type' => 'boolean',
    'default' => TRUE,
    'token' => TRUE,
    'access' => 'imlogistic connection settings',
  );

  $variables['commerce_iml_tokens_CustomerOrder'] = array(
    'title' => t('Order Id'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:order-id]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Contact'] = array(
    'title' => t('Customer Name'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:name-line]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Phone'] = array(
    'title' => t('Customer Phone'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:field-customer-phone]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Email'] = array(
    'title' => t('Customer Email'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:mail]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Address'] = array(
    'title' => t('Customer Address'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:thoroughfare]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_RegionCodeTo'] = array(
    'title' => t('Customer City'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:locality]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_RegionCodeFrom'] = array(
    'title' => t('Warehouse City'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Comment'] = array(
    'title' => t('Order Comment'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field_order_comment]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Amount'] = array(
    'title' => t('Amount to be collected'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:order-payment-balance-amount-decimal]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_ValuatedAmount'] = array(
    'title' => t('Order Valuated Amount (order total)'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-order-total:amount-decimal]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Weight'] = array(
    'title' => t('Order Weight'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '1',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_Volume'] = array(
    'title' => t('Number Of Allocated spaces'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '1',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_DeliveryDate'] = array(
    'title' => t('Delivery date'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-order-delivery-date:value:custom:d-m-Y]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_TimeFrom'] = array(
    'title' => t('Delivery Time period beginning'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-order-delivery-date:value:custom:H-i]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_TimeTo'] = array(
    'title' => t('Delivery Time period end'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-order-delivery-date:value2:custom:H-i]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_tokens_ServicePointExternalCode'] = array(
    'title' => t('Service point ExternalCode'),
    'description' => t('If order has non-empty code then order in considered for delivery to service point but not to customer address.'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:field-service-point:external-code]',
    'access' => 'imlogistic order tokens settings',
  );
  $variables['commerce_iml_token_BarCode'] = array(
    'title' => t('Order BarCode'),
    'description' => t('Barcode which is used to track the order.'),
    'group' => 'commerce_iml_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-parcel-code]',
    'access' => 'imlogistic order tokens settings',
  );

  $variables['commerce_iml_enable_post_terminal_pods'] = array(
    'title' => t('Enable Post Terminal PODs'),
    'group' => 'commerce_iml_pod_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'type' => 'boolean',
    'default' => TRUE,
    'token' => TRUE,
    'access' => 'imlogistic pod shipping settings',
  );

  $variables['commerce_iml_enable_post_shipping'] = array(
    'title' => t('Enable Post Shipping'),
    'group' => 'commerce_iml_post_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'type' => 'boolean',
    'default' => TRUE,
    'token' => TRUE,
    'access' => 'imlogistic post shipping settings',
  );
  $variables['commerce_iml_tokens_PostContentType'] = array(
    'title' => t('Post Content Type'),
    'group' => 'commerce_iml_post_settings',
    'element' => array(
      '#type' => 'select',
      '#options' => array(
        0 => 'Печатная',
        1 => 'Разное',
        2 => '1 Класс',
      ),
      '#size' => 1,
    ),
    'type' => 'number',
    'default' => 1,
    'access' => 'imlogistic post shipping settings',
  );
  $variables['commerce_iml_tokens_PostCode'] = array(
    'title' => t('Order PostCode Token'),
    'description' => t('Should only be present if order is to be delivered via State Post Service.'),
    'group' => 'commerce_iml_post_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:postal-code]',
    'access' => 'imlogistic post shipping settings',
  );

  $variables['commerce_iml_server_include_line_items'] = array(
    'title' => t('Include order line items information'),
    'group' => 'commerce_iml_order_line_items',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'type' => 'boolean',
    'default' => FALSE,
    'token' => TRUE,
    'access' => 'imlogistic order line items settings',
  );
  $variables['commerce_iml_product_token_BarCode'] = array(
    'title' => t('Order Product BarCode'),
    'description' => t('Barcode which is used to track the product. It is mandatory if line items info is passed to IML.'),
    'group' => 'commerce_iml_order_line_items',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-product:field-barcode]',
    'access' => 'imlogistic order line items settings',
  );
  $variables['commerce_iml_product_default_barcode'] = array(
    'title' => t('Default Product BarCode'),
    'description' => t('This value will be used if previous token wont resolve to a correct value.'),
    'group' => 'commerce_iml_order_line_items',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'type' => 'string',
    'default' => '9999999999',
    'access' => 'imlogistic order line items settings',
  );

  return $variables;
}
