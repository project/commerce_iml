<?php

/**
 * @file
 * IM Logistic Rules hooks, actions and triggers.
 * 
 */

function commerce_iml_rules_event_info() {
  $events = array();

  $events['commerce_iml_order_saved'] = array(
    'label' => t('Order just been saved in IML system'),
    'group' => t('Commerce IML'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce order'),
      ),
      'result' => array(
        'type' => 'struct',
        'label' => t('IML system saving results data'),
      ),
      'is_test_mode' => array(
        'type' => 'boolean',
        'label' => t('Is test mode'),
        'default value' => FALSE,
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );
  $events['commerce_iml_order_state_changed'] = array(
    'label' => t('Order state changed in IML system'),
    'group' => t('Commerce IML'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce order'),
      ),
      'order_info' => array(
        'type' => 'struct',
        'label' => t('IML system order info'),
      ),
      'is_test_mode' => array(
        'type' => 'boolean',
        'label' => t('Is test mode'),
        'default value' => FALSE,
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );
  $events['commerce_iml_service_point_changed'] = array(
    'label' => t('Service point information changed in IML'),
    'group' => t('Commerce IML'),
    'variables' => array(
      'service_point' => array(
        'type' => 'struct',
        'label' => t('Service point'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}


function commerce_iml_rules_action_info() {
  $actions = array();

  $actions['commerce_iml_save_order'] = array(
    'label' => t('Save order to IML system.'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'save' => FALSE,
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_iml_save_order_rules_action',
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $actions;
}

function commerce_iml_save_order_rules_action($commerce_order, $settings, RulesState $state, RulesPlugin $element) {
  $result = commerce_iml_submit_order($commerce_order);
  if ($result && isset($result['Result']) && $result['Result'] === 'OK') {
    watchdog('commerce_iml', 'Successfully submit order @order_id', array('@order_id' => $commerce_order->order_id), WATCHDOG_INFO);
  }
  elseif ($result) {
    watchdog('commerce_iml', 'Failed to register order @order_id: "@message"', array(
      '@order_id' => $commerce_order->order_id,
      '@message' => print_r($result['Errors'], TRUE),
    ), WATCHDOG_WARNING);
  }
  else {
    watchdog('commerce_iml', 'Order is not compatible with IML processing "<a href ="/admin/commerce/orders/@order_id/edit">@order_id</a>"', array(
      '@order_id' => $commerce_order->order_id,
    ), WATCHDOG_WARNING);
  }
}