<?php

/**
 * Sends requests to the server returns error if a timeout occurs.
 */
class ImlApi {

  /**
   * @var string
   *  Well-formed server url to connect with
   */
  private $serverUrl;

  /**
   * @var int
   *  Timeout for server communications in seconds.
   */
  private $timeout;

  /**
   * @var string
   *  Api login.
   */
  private $login;

  /**
   * @var string
   *  Api password.
   */
  private $password;

  /**
   * @var boolean
   *  If working in test mode.
   */
  private $isTestMode;

  /**
   * @param string $serverUrl
   *  Server url.
   * @param int $timeout
   *  Timeout in seconds.
   * @param string $login
   *  Api login.
   * @param string $password
   *  Api password.
   * @param boolean $isTestMode
   *  Is test mode.
   */
  public function __construct($serverUrl, $timeout, $login, $password, $isTestMode) {
    $this->serverUrl = $serverUrl;
    $this->timeout = $timeout;
    $this->login = $login;
    $this->password = $password;
    $this->isTestMode = $isTestMode;
  }

  /**
   * @param array $parameters
   *
   * @return array
   */
  public function getOrderStatuses(array $parameters) {
    return $this->sendPostRequest('GetStatuses', $parameters);
  }

  /**
   * @param array $parameters
   *
   * @return array
   */
  public function saveOrder(array $parameters) {
    return $this->sendPostRequest('CreateOrder', $parameters);
  }

  /**
   * @param string $barCode
   *
   * @return string
   */
  public function printBarcode($barCode) {
    return $this->sendGetRequest("PrintBar?Barcode={$barCode}");
  }

  /**
   * @param string[] $barcodes
   *
   * @return string
   */
  public function printBarcodes(array $barcodes) {
    $barcodesInfo = implode('|', $barcodes);
    return $this->sendGetRequest("PrintBar?Barcode={$barcodesInfo}");
  }

  /**
   * @param $urlSuffix
   *
   * @return mixed
   */
  private function sendGetRequest($urlSuffix) {
    $response = drupal_http_request("{$this->serverUrl}{$urlSuffix}", array(
      'method' => 'GET',
      'headers' => array(
        'Authorization' => 'Basic ' . base64_encode("{$this->login}:{$this->password}"),
        'Accept' => '*/*',
      ),
    ));

    if (!(isset($response->status_message) && $response->status_message == 'OK')) {
      return array('Errors' => array($response->error));
    }

    if (isset($response->headers['content-type']) && $response->headers['content-type'] == 'application/json; charset=utf-8') {
      return json_decode($response->data, TRUE);
    }

    return $response->data;
  }

  /**
   * @param string $urlSuffix
   * @param array $parameters
   *
   * @return array
   *  Resulted data structure representing decoded xml or containing error information.
   */
  private function sendPostRequest($urlSuffix, array $parameters) {
    if ($this->isTestMode) {
      $parameters['Test'] = 'True';
    }
    
    try {
      $requestOptions = array(
        'method' => 'POST',
        'timeout' => $this->timeout,
        'headers' => array(
          'Content-type' => 'application/x-www-form-urlencoded;charset="utf-8"',
          'Authorization' => 'Basic ' . base64_encode("{$this->login}:{$this->password}"),
        ),
        'data' => http_build_query($parameters),
      );

      $communicationStartTime = time();
      $response = drupal_http_request("{$this->serverUrl}{$urlSuffix}", $requestOptions);
      if (isset($response->error)) {
        $this->throwConnectionException($communicationStartTime, t("Problem occurred while connecting to API's server."));
      }

      if (!isset($response->data) || empty($response->data)) {
        $this->throwConnectionException($communicationStartTime, t("Problem occurred while reading data from API's server."));
      }

      return json_decode($response->data, TRUE);
    }
    catch (Exception $exception) {
      return array('Errors' => array($exception->getMessage()));
    }
  }

  /**
   * @param int $communicationStartTime
   *
   * @return bool
   */
  private function isTimedOut($communicationStartTime) {
    return (time() - $communicationStartTime) >= $this->timeout;
  }

  /**
   * @param int $communicationStartTime
   * @param string $message
   * 
   * @throws Exception
   */
  private function throwConnectionException($communicationStartTime, $message) {
    $timeoutMsg = $this->isTimedOut($communicationStartTime) ? t('API server response time exceeded (@timeout)', array('@timeout' => $this->timeout)) : '';
    throw new Exception($message . ' ' . $timeoutMsg);
  }
}
