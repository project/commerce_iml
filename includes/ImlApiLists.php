<?php

/**
 */
class ImlApiLists {

  /**
   * @var string
   *  Well-formed server url to connect with
   */
  private $serverUrl;

  /**
   * @var string
   *  Api login.
   */
  private $login;

  /**
   * @var string
   *  Api password.
   */
  private $password;

  /**
   * @param string $serverUrl
   *  Server url.
   * @param string $login
   *  Api login.
   * @param string $password
   *  Api password.
   */
  public function __construct($serverUrl, $login, $password) {
    $this->serverUrl = $serverUrl;
    $this->login = $login;
    $this->password = $password;
  }

  /**
   * @param string $listName
   *
   * @return array
   */
  public function requestList($listName) {
    $response = drupal_http_request("{$this->serverUrl}{$listName}", array(
      'headers' => array(
        'Authorization' => "Basic " . base64_encode("{$this->login}:{$this->password}"),
        'Accept' => '*/*',
      ),
      'method' => 'GET',
    ));

    return empty($response->error) ? json_decode($response->data, TRUE) : array();
  }

}
